

-- принимает имя файла и таблицу и сохраняет в файл кусок данных размером определенным params
-- params это 4 инта
-- номер циллиндра, номер сектора, кол-во циллиндров, кол-во секторов
local function printRect(filename, params)
        local start_cillinder , start_sector, count_cillinders, count_sectors = unpack(params);
        count_cillinders = start_cillinder + count_cillinders
        count_sectors = start_sector + count_sectors

        local file = assert(io.open(filename,"r"), "can't open file " .. filename)
        local temp = file:read("*a")

        sectors = {}
        for value in string.gmatch(temp,"%d+") do
                sectors[#sectors + 1] = value
        end

        local new_filename = filename .. "_output_" .. start_sector .. "_" .. start_cillinder .. "_" .. count_sectors .. "_" .. count_cillinders
        local file_output = assert(io.open(new_filename, "w"), "can't open file " .. filename)
        for sector = start_sector, count_sectors do
                file_output:write(string.sub(sectors[sector], start_cillinder, count_cillinders))
                file_output:write("\n")
        end
        file_output:close()
        file:close()
        print("saved by " .. new_filename)
end


--printRect("/home/svetlana/projects/indicator/logs/filter_data_50_36", { 785, 440, 50, 50})
printRect(arg[1], { arg[2], arg[3], arg[4], arg[5]})
